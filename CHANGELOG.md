# Soong\Console

[![Latest Version on Packagist][ico-version]][link-packagist]
[![Software License][ico-license]](LICENSE.md)
[![Build Status][ico-travis]][link-travis]
[![Coverage Status][ico-scrutinizer]][link-scrutinizer]
[![Quality Score][ico-code-quality]][link-code-quality]
[![Total Downloads][ico-downloads]][link-downloads]

soong\console provides basic Symfony Console commands for using the Soong data migration framework. At the moment, the implementation is very basic, for testing and demonstration purposes - it needs to be fleshed out to provide more flexible functionality.

## Install

Via Composer

``` bash
$ composer require soong/console
```

## Usage


## Demos

To setup for demoing Soong ETL:

1. Create an empty database for testing.
1. Import `data/extractsource.sql` to the database (table to be populated by the first demo).
1. Import `data/beer.sql` to the database (tables to be populated for the second demo).
1. Edit each of the files in `config/` - where indicated, replace the sample credentials with those for the test database.

Demo 1:

1. Execute `bin/soong migrate arraytosql`
1. Look at the `extractsource` table to see the data populated, and that the ids have been assigned consecutively.
1. Look at the `map_arraytosql` table to see the mapping from source to destination keys.
1. Execute `bin/soong migrate sqltocsv`
1. Observe CSV data output to the terminal with configured transformations applied.
1. Execute `bin/soong rollback arraytosql`
1. Observe that the `extractsource` and `map_arraytosql` tables are now empty.

Demo 2:

1. Execute `bin/soong migrate beertopics`
1. Observe the `beer_terms` table is populated from CSV data - in particular, see how the 'red ale' reference to its 'ale' parent has been converted to the numeric ID assigned to the 'ale' row in the database.
1. Execute `bin/soong migrate beeraccounts`
1. Observe the `beer_users` table - in particular, see how the `ValueLookup` transformer converted the boolean values in the `pro` column to strings in the `taster` column.
1. Execute `bin/soong migrate beercontent`
1. Observe the `beer` table - in particular, see how the relationships to users/accounts was maintained even though the IDs for the users changed (also see the `map_beeraccounts` table).
1. Execute `bin/soong rollback beercontent`
1. Observe how the `beer` and `map_beercontent` tables are now empty.


## Change log

Please see [CHANGELOG](CHANGELOG.md) for more information on what has changed recently.

## Testing

``` bash
$ composer test
```

## Contributing

Please see [CONTRIBUTING](CONTRIBUTING.md) and [CODE_OF_CONDUCT](CODE_OF_CONDUCT.md) for details.

### Todo
* Complete test coverage
* Customize separator
* Customize output file/stream

## Security

If you discover any security related issues, please email `soong@virtuoso-performance.com` instead of using the issue tracker.

## Credits

- [Mike Ryan][link-author]
- [All Contributors][link-contributors]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-version]: https://img.shields.io/packagist/v/soong/console.svg?style=flat-square
[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square
[ico-travis]: https://img.shields.io/travis/soong/console/master.svg?style=flat-square
[ico-scrutinizer]: https://img.shields.io/scrutinizer/coverage/g/soong/console.svg?style=flat-square
[ico-code-quality]: https://img.shields.io/scrutinizer/g/soong/console.svg?style=flat-square
[ico-downloads]: https://img.shields.io/packagist/dt/soong/console.svg?style=flat-square

[link-packagist]: https://packagist.org/packages/soong/console
[link-travis]: https://travis-ci.org/soong/console
[link-scrutinizer]: https://scrutinizer-ci.com/g/soong/console/code-structure
[link-code-quality]: https://scrutinizer-ci.com/g/soong/console
[link-downloads]: https://packagist.org/packages/soong/console
[link-author]: https://gitlab.com/mikeryan776
[link-contributors]: ../../contributors

<?php

namespace Soong\Console\Tests\Command;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Tester\CommandTester;

/**
 * Tests the \Soong\Console\Coommand\MigrateCommand class.
 */
abstract class CommandTestBase extends TestCase
{

    /**
     * Fully qualified name of the command class to test.
     *
     * @var string
     */
    protected $commandClass;

    /**
     * Command name being tested.
     *
     * @var string
     */
    protected $commandName;

    /**
     * Command testing object.
     *
     * @var CommandTester
     */
    protected $commandTester;

    /**
     * @inheritdoc
     */
    public function setUp(): void
    {
        parent::setUp();
        $application = new Application('Soong POC', '0.1.0');
        $application->add(new $this->commandClass());
        $command = $application->find($this->commandName);
        $this->commandTester = new CommandTester($command);
    }
}

<?php
declare(strict_types=1);

namespace Soong\Console\Transformer\Property;

use Soong\Contracts\Exception\PropertyTransformerException;
use Soong\Transformer\Property\PropertyTransformerBase;

/**
 * PropertyTransformer to uppercase the first letter of the extracted data.
 */
class UcFirst extends PropertyTransformerBase
{

    /**
     * @inheritdoc
     */
    public function __invoke($data) : ?string
    {
        if (is_null($data)) {
            return null;
        }
        if (!is_string($data)) {
            throw new PropertyTransformerException(
                "UcFirst property transformer: expected string value, received " .
                gettype($data)
            );
        }

        return ucfirst($data);
    }
}
